import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { colorModel } from './notes.model';

// To display the color list in the color drop down field
let colorList = [{
    id: 1,
    name: "Red",
    code: "#FF0000",
    class: 'border-red'
  },
  {
    id: 2,
    name: "Blue",
    code: "#0000FF",
    class: 'border-blue'
  },
  {
    id: 3,
    name: "Orange",
    code: "#FFA500",
    class: 'border-orange'
  },
  {
    id: 4,
    name: "Green",
    code: "#008000",
    class: 'border-green'
  },
  {
    id: 5,
    name: "Purple",
    code: "#800080",
    class: 'border-purple'
  },
  {
    id: 6,
    name: "Yellow",
    code: "#FFFF00",
    class: 'border-yellow'
  }
]

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss']
})
export class NotesComponent implements OnInit {

  colorList: colorModel[] = colorList;
  selectedColor: any = undefined;
  noteName = new FormControl('');
  noteColor = new FormControl('');
  lastEdit: any = '';
  notesList: any = [];
  isValidation: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private Router: Router
  ) {}

  ngOnInit() {
  }

  // To get the color class to append the class based on the color selection 
  getColorClass(note) {
    let noteColor = '';
    for (let eachColor of this.colorList) {
      if (note.color === eachColor.id) {
        noteColor = eachColor['class'];
      }
    }
    return noteColor;
  }

  // Get the form filed to list the note content
  addNote() {
    this.getValidationStatus();
    let noteName = this.noteName.value;
    let noteColor = this.noteColor.value;
    let sno = this.notesList.length + 1;
    let noteObj = { date: this.formatDate(new Date()), id: sno, title: `Note ${sno}`, color: noteColor, description: noteName };
    this.notesList.unshift(noteObj);
    this.resetForm();
  }

  // to get the formated date
  formatDate(date) {
    var monthNames = [
      "January", "February", "March",
      "April", "May", "June", "July",
      "August", "September", "October",
      "November", "December"
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    var hours = date.getHours();
    var minutes = date.getMinutes();
    return monthNames[monthIndex] + ' ' + day + ', ' + hours + ':' + minutes;
  }

  // Edit the notes when user clicks the edit icon
  editNote(note) {
    this.noteName.setValue(note.description);
    this.noteColor.setValue(note.color);
    this.lastEdit = note;
  }

  updateNote() {
    this.getValidationStatus();
    let noteName = this.noteName.value;
    let noteColor = this.noteColor.value;
    for (let eachNode of this.notesList) {
      if (this.lastEdit.id === eachNode.id) {
        eachNode.description = noteName;
        eachNode.color = noteColor;
      }
    }
    this.lastEdit = '';
    this.resetForm();
  }

  // Rest the form
  resetForm() {
    this.noteName.setValue('');
    this.noteColor.setValue(undefined);
  }

  // Delete the note item when user clicks on delete
  deleteNote(note) {
    let newNotes = [];
    const index: number = this.notesList.indexOf(note);
    if (index !== -1) {
      this.notesList.splice(index, 1);
    }
    this.isValidation = false;
  }

  getValidationStatus() {
    if (this.noteName.value != '' && this.noteColor.value != undefined) {
      this.isValidation = true;
    }
}


}
