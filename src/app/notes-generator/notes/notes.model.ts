export interface colorModel {
  id: number;
  name: string;
  code: string;
};