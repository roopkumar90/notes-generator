import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Include the components for routing
const routes: Routes = [
  {
    path: 'notes',
    loadChildren: './notes-generator/notes-generator.module#NotesGeneratorModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
